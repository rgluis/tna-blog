<?php
get_header('single');
?>
    <section class="single">
        <div class="row">
            <article class="post">
                <div class="post__featured-image">
                    <?php the_post_thumbnail('full'); ?>
                </div>

                <div class="post__title">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="post__author">
                    <h5>Por: <?php the_author(); ?></h5>
                </div>

                <div class="post__content">
                    <?php the_content(); ?>
                </div>

                <div class="post__comments">
                    <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
                </div>
            </article>
            <section class="sidebar">
                <div class="post__date">
                    <p><?php the_date(); ?></p>
                </div>
                <?php
                    $terms = get_terms(array(
                        'taxonomy' => 'category',
                        'orderby' => 'slug',
                        'order' => 'ASC',
                        //'parent' => '1',
                        'hide_empty' => false
                    ));
                    if(count($terms) > 0) :
                        ?>
                        <div class="sidebar__categories">
                            <p class="sidebar__categories__title">Categorías:</p>
                            <ul>
                                <?php
                                foreach ($terms as $term) {
                                    $categoryLink = get_category_link($term->term_id);
                                    ?>
                                    <li><a href="<?php echo $categoryLink; ?>"><?php echo $term->name; ?></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    <?php
                    endif;
                    ?>

                <div class="sidebar__subscription">
                    <div class="sidebar__subscription__title">
                        <p>Suscríbete</p>
                    </div>
                    <div class="sidebar__subscription__form">
                        <div class="inner">
                            <form action="" id="form_sidebar">
                                <input type="hidden" name="tipo" value="form_subscriber_sidebar">
                                <div class="form-control">
                                    <input type="email" class="email" name="email" placeholder="Email" required>
                                    <button type="submit"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/large-arrow.svg" alt=""></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="sidebar__social">
                    <ul class="social--blue">
                        <?php
                        $urlFacebook = get_field('url_facebook','option');
                        $urlBehance = get_field('url_behance','option');
                        $urlLinkedin = get_field('url_linkedin','option');
                        $urlInstagram = get_field('url_instagram','option');

                        if(!empty($urlFacebook)) :
                            ?>
                            <li><a href="<?php echo $urlFacebook; ?>" target="_blank"><span class="icon-facebook"></span></a></li>
                        <?php
                        endif;
                        if(!empty($urlBehance)) :
                            ?>
                            <li><a href="<?php echo $urlBehance; ?>" target="_blank"><span class="icon-behance"></span></a></li>
                        <?php
                        endif;
                        if(!empty($urlLinkedin)) :
                            ?>
                            <li><a href="<?php echo $urlLinkedin; ?>"target="_blank"><span class="icon-linkedin"></span></a></li>
                        <?php
                        endif;
                        if(!empty($urlInstagram)) :
                            ?>
                            <li><a href="<?php echo $urlInstagram; ?>" target="_blank"><span class="icon-instagram"></span></a></li>
                        <?php
                        endif;
                        ?>
                    </ul>
                </div>
            </section>
        </div>
    </section>

    <section class="subscription--blue">
        <div class="row">
            <div class="subscription__title">
                <h2>¿Te gusta nuestro blog?</h2>
                <p>Suscríbete para recibir noticias</p>
            </div>
        </div>
        <div class="row">
            <div class="subscription__form">
                <div class="inner">
                    <form action="" id="form">
                        <div class="form-control">
                            <input type="email" class="email" name="email" placeholder="Email" required>
                            <button type="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
?>