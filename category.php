<?php
get_header();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
?>
    <section class="categories">
        <div class="row">
            <div class="categories__title">
                <h1>Categoría: <?php single_cat_title(); ?></h1>
            </div>
        </div>
    </section>
    <section class="articles-feed">
        <div class="row">
            <?php
            while(have_posts()) : the_post();
                ?>
                <article class="article-feed">
                    <a href="<?php the_permalink(); ?>">
                        <div class="article-feed__background" style="background-image: url('<?php echo the_post_thumbnail_url('postfeed'); ?>');"></div>
                        <div class="article-feed__content">
                            <div class="article-feed__content__text-container">
                                <div class="article-feed__content__category">
                                    <?php
                                    $post_categories = get_the_category(get_the_ID());
                                    //var_dump($post_categories);
                                    ?>
                                    <p>
                                        <?php
                                        $str = implode(', ', array_map(function($cat) {
                                            return $cat->name;
                                        }, $post_categories));
                                        echo $str;
                                        ?>
                                    </p>
                                </div>
                                <div class="article-feed__content__title">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                                <div class="article-feed__content__excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </article>

            <?php
            endwhile;
            ?>
        </div>
    </section>

<?php
$big = 999999999; // need an unlikely integer
?>
    <section class="pagination">
        <div class="row">
            <?php
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'prev_next'          => true,
                'prev_text'          => __('« '),
                'next_text'          => __(' »'),
                'after_page_number'  => '',
                'type'               => 'list',
            ) );
            ?>
        </div>
    </section>

    <section class="subscription--red">
        <div class="row">
            <div class="subscription__title">
                <h2>¿Te gusta nuestro blog?</h2>
                <p>Suscríbete para recibir noticias</p>
            </div>
        </div>
        <div class="row">
            <div class="subscription__form">
                <div class="inner">
                    <form action="" id="form">
                        <div class="form-control">
                            <input type="email" id="email" name="email" placeholder="Email" required>
                            <button type="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
?>