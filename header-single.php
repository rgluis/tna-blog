<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <?php wp_head(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=7,8,edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/slick-carousel/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/slick-carousel/slick/slick-theme.css"/>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/wow/css/libs/animate.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/intl-tel-input/build/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/styles.css">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#ff5050">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon.ico">
    <meta name="msapplication-config" content="<?php echo get_stylesheet_directory_uri(); ?>/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KJC7HWC');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="body" class="wow fadeIn" data-wow-delay="0.2s">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KJC7HWC"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.11&appId=906830189470365';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<header class="header--red">
    <div class="row">
        <div class="header__logo">
            <span class="icon-tna red"></span>
        </div>

        <div class="header__main-menu">
            <div class="header__main-menu__toggle">
                <a href="" class="" id="nav-toggle"><span class="icon-menu"></span></a>
            </div>

            <?php
            $defaults = array(
                'theme_location'  => 'top-menu',
                'menu'            => '',
                'container'       => false,
                'container_class' => '',
                'container_id'    => '',
                'menu_class'    => '',
                'menu_id'         => 'nav-menu',
                'echo'            => true,
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 2,
                'walker'          => new Embassy_Walker_Nav_Menu()
            );

            wp_nav_menu( $defaults );
            ?>
        </div>

        <div class="header__social">
            <ul class="social">
                <?php
                $urlFacebook = get_field('url_facebook','option');
                $urlBehance = get_field('url_behance','option');
                $urlLinkedin = get_field('url_linkedin','option');
                $urlInstagram = get_field('url_instagram','option');

                if(!empty($urlFacebook)) :
                    ?>
                    <li><a href="<?php echo $urlFacebook; ?>" target="_blank"><span class="icon-facebook"></span></a></li>
                <?php
                endif;
                if(!empty($urlBehance)) :
                    ?>
                    <li><a href="<?php echo $urlBehance; ?>" target="_blank"><span class="icon-behance"></span></a></li>
                <?php
                endif;
                if(!empty($urlLinkedin)) :
                    ?>
                    <li><a href="<?php echo $urlLinkedin; ?>"target="_blank"><span class="icon-linkedin"></span></a></li>
                <?php
                endif;
                if(!empty($urlInstagram)) :
                    ?>
                    <li><a href="<?php echo $urlInstagram; ?>" target="_blank"><span class="icon-instagram"></span></a></li>
                <?php
                endif;
                ?>
            </ul>
        </div>
    </div>
</header>