<?php
function my_init()
{
    if (!is_admin())
    {
        wp_deregister_script('jquery');

        // Load the copy of jQuery that comes with WordPress
        // The last parameter set to TRUE states that it should be loaded
        // in the footer.
        wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', FALSE, '1.11.0', TRUE);

        wp_enqueue_script('jquery');
    }

    //add_filter('acf/settings/show_admin', '__return_false');

    //require_once 'vendor/acf-fields/index.php';
}
add_action('init', 'my_init');
add_theme_support('post-thumbnails');
add_theme_support('title-tag');

register_nav_menus(array(
    'top-menu'      => 'Top Menu',
    'footer-menu'      => 'Footer Menu',
));

class Embassy_Walker_Nav_Menu extends Walker_Nav_Menu {

    private $parametros;

    function __construct($parametros = null)
    {
        $this->parametros = $parametros;
    }

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n" . $indent ."<ul" . ($this->parametros != '' ? " class='". $this->parametros ."'" : '') . ">" . "\n";
    }
}

add_filter('upload_mimes', 'personal_upload_mimes');

function personal_upload_mimes($mimes = array()) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Opciones Generales',
        'menu_title'    => 'Opciones Generales',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

}

// AJAX
function form_subscriber() {

    if(is_email($_POST["email"])) {
        global $wpdb;
        $wpdb->insert('leads', [
            'tipo'          => (isset($_POST["tipo"]) && !empty($_POST["tipo"])) ? $_POST["tipo"] : 'form_subscriber',
            'email'         => $_POST["email"],
            "url"           => $_POST["url"],
            "utm_source"    => $_POST["utm_source"],
            "utm_medium"    => $_POST["utm_medium"],
            "utm_term"      => $_POST["utm_term"],
            "utm_content"   => $_POST["utm_content"],
            "utm_campaign"  => $_POST["utm_campaign"],
            "gclid"         => $_POST["gclid"],
            "cid"           => $_POST["cid"]
        ],['%s','%s','%s','%s','%s','%s','%s','%s','%s','%s']);
        status_header(200);
        echo json_encode(["valid" => true, "msg" => "Correcto"]);
    }else {
        status_header(422);
        echo json_encode(["valid" => false, "msg" => "Ingrese un correo electrónico."]);
    }
    wp_die();
}
add_action('wp_ajax_form_subscriber', 'form_subscriber');
add_action('wp_ajax_nopriv_form_subscriber', 'form_subscriber');


/**
 * Desactivar emoticons en wordpress
 */
function disable_wp_emojicons() {

    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

add_image_size( 'poststiky', 1280);
add_image_size( 'postfeed', 700);
/*add_image_size( 'galleryfeed', 400, 400, array( 'center', 'center' ) );*/


function custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
        return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/**
 * Actualizar shortcode de video
 */
function update_shortcode_video( $value ) {
    $post_id = get_the_ID();
    $value["value"] = "[video-hc id='".$post_id."']";
    return $value;
}
add_filter('acf/load_field/key=field_59d1b8edaf116', 'update_shortcode_video', 10, 3);

/**
 * Actualizar shortcode de CTA
 */
function update_shortcode_cta( $value ) {
    $post_id = get_the_ID();
    $value["value"] = "[cta-hc id='".$post_id."']";
    return $value;
}
add_filter('acf/load_field/key=field_59d1ce247b66d', 'update_shortcode_cta', 10, 3);

add_shortcode('video-hc', 'shortcode_video_hc');
function shortcode_video_hc($atts, $content = null) {
    extract( shortcode_atts( array(
        'id' => '',
    ), $atts ) );

    $backgroundImage = get_field('video_imagen_previa',$id);
    $youtubeURL = get_field('video_youtube_url',$id);
    $partsURL = parse_url($youtubeURL);
    parse_str($partsURL["query"],$query);

    $youtubeVideoID = $query["v"];

    $html = "<div class='video-player'>
                    <div class='video-preview' style='background-image: url(\"".$backgroundImage."\");'>
                        <div class='video-play-button'><img src='".get_stylesheet_directory_uri()."/img/play-button.png'></div>
                    </div>
                    <div class='video-container'>
                        <iframe id='video".$id."' src='https://www.youtube.com/embed/".$youtubeVideoID."?enablejsapi=1' frameborder='0'></iframe>
                    </div>
                </div>";

    return $html;
}

add_shortcode('cta-hc', 'shortcode_cta_hc');
function shortcode_cta_hc($atts, $content = null) {
    extract( shortcode_atts( array(
        'id' => '',
    ), $atts ) );

    $ctaLink = get_field('call_to_action_link', $id);
    $imageDesktop = get_field('call_to_action_image_desktop', $id);
    $imageMobile = get_field('call_to_action_image_mobile', $id);


    $html = "<div class='single-post__cta' data-title='".get_the_title($id)."' data-images='[\"".$imageMobile['url']."\",\"". $imageDesktop['url'] ."\"]'>
                    <a href='$ctaLink' target='_blank'></a>
                </div>";

    return $html;
}

function search_filter($query) {
    if ( !is_admin() && $query->is_main_query() ) {
        if ($query->is_search) {
            $query->set('post_type', array( 'post' ) );
        }
    }
}

add_action('pre_get_posts','search_filter');