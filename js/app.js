((function($){
    //edgrid.menu('main-nav','main-menu');
    new WOW().init();

    var $screenWidth =  window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;

    var $screenHeight = window.innerHeight
        || document.documentElement.clientHeight;

    $('.hero__slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: "<div class='prev-arrow'></div>",
        nextArrow: "<div class='next-arrow'></div>",
        appendArrows: $("#arrow-container"),
        appendDots: $("#arrow-container"),
        dotsClass: "slick-points",
        autoplay: true,
        autoplaySpeed: 4500,
        speed: 1000,
    });

    /**
     * Animaciones
     */
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
            });
            return this;
        }
    });

    /**
     * Serialize JSON
     * @returns {{}}
     */
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }
    function createCookie(name, value, days) {
        var date, expires, path = window.location.pathname;
        if (days) {
            date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name+"="+value+expires+"; path="+path;
    }

    var utms = function (nombreDeFormulario) {
        var formulario = $("#" + nombreDeFormulario),
            utms = ["url", "utm_source", "utm_medium", "utm_term", "utm_content", "utm_campaign", "gclid", "cid"],
            names = ["url", "utm_source", "utm_medium", "utm_term", "utm_content", "utm_campaign", "gclid", "cid"];

        try {
            var tracker = ga.getAll()[0], cid = tracker.get('clientId');
        } catch (e) {
            var cid = null;
        }

        for (i = 0; i < utms.length; i++) {
            formulario.append('<input type="hidden" id="' + utms[i] + '" name="' + names[i] + '" value="' + getCookie(utms[i]) + '">');
            if (utms[i] == 'cid' || utms[i] == 'url') {
                if ($("#cid")) {
                    $("#cid").val(cid);
                }
                if ($("#url")) {
                    $("#url").val(document.URL);
                }
            }
        }
    };
    jQuery.validator.addMethod("phone_intl", function(value, element){
        if($(element).intlTelInput("isValidNumber")) {
            var valor = $(element).intlTelInput("getNumber");
            $("#" + $(element).data('hidden-value')).val(valor);
            return true;
        }else {
            return false;
        }
    });

    $("#tel").intlTelInput({
        utilsScript: 'bower_components/intl-tel-input/build/js/utils.js',
        preferredCountries: ['mx','us'],
        initialCountry: 'mx',
        autoPlaceholder: 'off',
        geoIpLookup: function(callback) {
            $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        }
    });

    $.extend($.validator.messages, {
        required: 'Campo obligatorio'
    });


    if(document.getElementById('form')) {
        var $form = $("#form");
        var $button = $form.find("button[type=submit]");

        $button.on("click", function(){
            $form.submit();
        });

        $form.validate({
            highlight: function(element) {
                $(element).closest('.form-control').addClass('error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('error');
            },
            errorPlacement: function(error, element) {
                element.attr("placeholder", error.text());
            },
            submitHandler: function(form) {
                var $form = $(form);

                utms($form.attr('id'));

                var $data = $form.serializeObject();
                $data.action = 'form_subscriber';

                $.ajax({
                    data:  $data,
                    url:   ajaxurl,
                    type:  'POST',
                    beforeSend: function () {
                        var height = $button.attr('disabled', 'disabled')
                            .height();

                        $button.html('<img src="'+path+'img/loading.svg" style="height:'+ height +'px !important;">');
                    },
                    error: function (request, status, error) {
                        console.log(status);
                        console.log(error);
                        console.log(request.responseText);
                    },
                    success:  function (response) {
                        var data = JSON.parse(response);
                        if(data.valid) {
                            $form.closest('.inner').fadeOut(function(){
                                $(this).html("<div class='inner-msg'><p>¡Gracias por suscribirte!</p></div>");
                            }).fadeIn();
                        }else {
                            $button.attr('disabled', '');
                            $form.find(".email").closest('.form-control').addClass('error');
                            $button.html('Enviar');
                        }
                    }
                });

                dataLayer.push({
                    'event': 'GAEvent',
                    'eventCategory' : 'Form',
                    'eventAction' : 'Send',
                    'eventLabel' : 'Subscription',
                    'eventValue' : undefined
                });

            },
            invalidHandler: function(event, validator) {
                $form.animateCss('shake');
            }
        });
    }//document.getElementById('form')

    if(document.getElementById('form_sidebar')) {
        var $formSidebar = $("#form_sidebar");
        var $buttonSidebar = $formSidebar.find("button[type=submit]");

        $buttonSidebar.on("click", function(){
            $formSidebar.submit();
        });

        $formSidebar.validate({
            highlight: function(element) {
                $(element).closest('.form-control').addClass('error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('error');
            },
            errorPlacement: function(error, element) {
                element.attr("placeholder", error.text());
            },
            submitHandler: function(form) {
                var $formSidebar = $(form);

                utms($formSidebar.attr('id'));

                var $data = $formSidebar.serializeObject();
                $data.action = 'form_subscriber';

                $.ajax({
                    data:  $data,
                    url:   ajaxurl,
                    type:  'POST',
                    beforeSend: function () {
                        var height = $buttonSidebar.attr('disabled', 'disabled')
                            .height();

                        $buttonSidebar.html('<img src="'+path+'img/loading.svg" style="height:'+ height +'px !important;">');
                    },
                    error: function (request, status, error) {
                        console.log(status);
                        console.log(error);
                        console.log(request.responseText);
                    },
                    success:  function (response) {
                        var data = JSON.parse(response);
                        if(data.valid) {
                            $formSidebar.closest('.inner').fadeOut(function(){
                                $(this).html("<div class='inner-msg'><p>¡Gracias por suscribirte!</p></div>");
                            }).fadeIn();
                        }else {
                            $buttonSidebar.attr('disabled', '');
                            $formSidebar.find(".email").closest('.form-control').addClass('error');
                            $buttonSidebar.html('Enviar');
                        }
                    }
                });

                dataLayer.push({
                    'event': 'GAEvent',
                    'eventCategory' : 'Form',
                    'eventAction' : 'Send',
                    'eventLabel' : 'Subscription Sidebar',
                    'eventValue' : undefined
                });

            },
            invalidHandler: function(event, validator) {
                $form.animateCss('shake');
            }
        });
    }//document.getElementById('form')

    $('.cta-go').on('click',function (e) {
        e.preventDefault();
        var selector;
        if(screen.width < 640) {
            selector = "data-click-small";
        }else {
            selector = "data-click-medium";
        }

        var target = $(this).attr(selector);
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

    var $navToggle = $("#nav-toggle");
    var $navMenu = $("#nav-menu");
    $navToggle.on("click", function(e){
        e.preventDefault();
        e.stopPropagation();

        $(this).toggleClass('active');
        $navMenu.toggleClass('active');
    });
    $(window).click(function() {
        $navMenu.removeClass('active');
        $navToggle.removeClass('active');
    });
    $navMenu.on("click", function(e) {
        var target = $( e.target );
        if ( target.is( "ul" ) ) {
            e.stopPropagation();
            e.preventDefault();
        }
    })

    /**
     * Call to actions
     */
    var $cta = $(".single-post__cta");
    $cta.each(function(index,element){
        var $container = $(element);
        var $arrImages = $container.attr("data-images");
        var $images = JSON.parse($arrImages);
        var $imageLoad = new Image();

        if($screenWidth < 640)
        {
            var url = $images[0];

        }else {
            var url = $images[1];
        }
        $imageLoad.src = url;
        $imageLoad.onload = function () {
            $container.find("a").append($imageLoad);
        };
    });

    $cta.on("click", function(e){
        dataLayer.push({
            'event': 'GAEvent',
            'eventCategory' : 'CTA',
            'eventAction' : 'Click',
            'eventLabel' : $(this).data('title'),
            'eventValue' : undefined
        });
    });

    /**
     * Pin it
     */
    $(document).on("click", ".pinterest", function(){
        var $imgSource = $(this).find("img").attr("src");
        PinUtils.pinOne({
            media: $imgSource,
            description: $(".post__title > h1").html()
        });

        dataLayer.push({
            'event': 'GAEvent',
            'eventCategory' : 'Pinterest',
            'eventAction' : 'Click',
            'eventLabel' : $imgSource,
            'eventValue' : undefined
        });
    });
})(jQuery));


/**
 * Load videos Youtube
 */
function getFrameID(id) {
    var elem = document.getElementById(id);
    if (elem) {
        if (/^iframe$/i.test(elem.tagName)) return id; //Frame, OK
        // else: Look for frame
        var elems = elem.getElementsByTagName("iframe");
        if (!elems.length) return null; //No iframe found, FAILURE
        for (var i = 0; i < elems.length; i++) {
            if (/^https?:\/\/(?:www\.)?youtube(?:-nocookie)?\.com(\/|$)/i.test(elems[i].src)) break;
        }
        elem = elems[i]; //The only, or the best iFrame
        if (elem.id) return elem.id; //Existing ID, return it
        // else: Create a new ID
        do { //Keep postfixing `-frame` until the ID is unique
            id += "-frame";
        } while (document.getElementById(id));
        elem.id = id;
        return id;
    }
    // If no element, return null.
    return null;
}

// Define YT_ready function.
var YT_ready = (function() {
    var onReady_funcs = [],
        api_isReady = false;
    /* @param func function     Function to execute on ready
     * @param func Boolean      If true, all qeued functions are executed
     * @param b_before Boolean  If true, the func will added to the first
     position in the queue*/
    return function(func, b_before) {
        if (func === true) {
            api_isReady = true;
            for (var i = 0; i < onReady_funcs.length; i++) {
                // Removes the first func from the array, and execute func
                onReady_funcs.shift()();
            }
        }
        else if (typeof func == "function") {
            if (api_isReady) func();
            else onReady_funcs[b_before ? "unshift" : "push"](func);
        }
    };
})();
// This function will be called when the API is fully loaded

function onYouTubePlayerAPIReady() {
    YT_ready(true);
}

var players = {};
//Define a player storage object, to enable later function calls,
//  without having to create a new class instance again.
YT_ready(function() {
    jQuery(".video-container iframe[id]").each(function() {
        var identifier = this.id;
        var frameID = getFrameID(identifier);
        if (frameID) { //If the frame exists
            players[frameID] = new YT.Player(frameID, {
                events: {
                    "onReady": createYTEvent(frameID, identifier),
                    "onStateChange": stopCycle
                }
            });
        }
    });
});

// Returns a function to enable multiple events
function createYTEvent(frameID, identifier) {
    return function (event) {
        var player = players[frameID]; // player object
        var the_div = jQuery('#'+identifier).parent().parent();
        the_div.find('.video-play-button').click(function() {
            var $this = jQuery(this);
            var $buttonParent = $this.parent();
            var $video = $this.parent().parent().find("iframe");
            $this.fadeOut();
            $buttonParent.fadeOut();
            $video.addClass('play');
            if ($video.hasClass('play')) {
                player.playVideo();

                dataLayer.push({
                    'event': 'GAEvent',
                    'eventCategory' : 'YouTube',
                    'eventAction' : 'Play',
                    'eventLabel' : player.getVideoData().title,
                    'eventValue' : undefined
                });

            }
        });
    }
}

function stopCycle(event) {
    if(event.data === 2) {
        event.target.pauseVideo();
    }
}

// Load YouTube Frame API
(function(){ //Closure, to not leak to the scope
    var s = document.createElement("script");
    s.src = "//www.youtube.com/player_api"; /* Load Player API*/
    var before = document.getElementsByTagName("script")[0];
    before.parentNode.insertBefore(s, before);
})();

jQuery( window ).on("load", function() {
    jQuery(".post img[class*='wp-']").one("load", function(){
        var $width = jQuery(this).width();
        var $height = jQuery(this).height();
        var $height2 = jQuery(this).outerHeight();
        var $classes = jQuery(this).attr("class");
        jQuery(this).wrap("<div class='pinterest "+ $classes +"' style='width: "+$width+"px;height:"+$height+"px;'></div>");
    }).each(function() {
        if(this.complete) {
            jQuery(this).trigger('load');
        }
    });
});