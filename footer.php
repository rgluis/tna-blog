<footer class="footer">
    <div class="footer__copyright">
        <p>THE NEW AGENCY ® <?php echo date("Y"); ?></p>
    </div>
    <div class="footer__info">
        <div class="row">
            <?php

                foreach(get_field('columna_footer','option') as $columna) :
            ?>
                    <div class="footer__info__item">
                        <p class="footer__info__item__title"><?php echo $columna["titulo_columna_footer"]; ?></p>
                        <div class="direction">
                            <?php echo $columna["cuerpo_columna_footer"]; ?>
                        </div>
                    </div>
            <?php
                endforeach;
            ?>

            <div class="footer__info__item">
                <div class="footer__info__item__logo">
                    <span class="icon-tna gray"></span>
                </div>
                <p class="footer__info__item__title">FOLLOW US!</p>
                <?php
                $defaults = array(
                    'theme_location'  => 'footer-menu',
                    'menu'            => '',
                    'container'       => false,
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'    => 'links',
                    'menu_id'         => '',
                    'echo'            => true,
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 1,
                    'walker'          => new Embassy_Walker_Nav_Menu()
                );

                wp_nav_menu( $defaults );
                ?>
                <ul class="social--footer">
                    <?php
                    $urlFacebook = get_field('url_facebook','option');
                    $urlBehance = get_field('url_behance','option');
                    $urlLinkedin = get_field('url_linkedin','option');
                    $urlInstagram = get_field('url_instagram','option');

                    if(!empty($urlFacebook)) :
                        ?>
                        <li><a href="<?php echo $urlFacebook; ?>" target="_blank"><span class="icon-facebook"></span></a></li>
                    <?php
                    endif;
                    if(!empty($urlBehance)) :
                        ?>
                        <li><a href="<?php echo $urlBehance; ?>" target="_blank"><span class="icon-behance"></span></a></li>
                    <?php
                    endif;
                    if(!empty($urlLinkedin)) :
                        ?>
                        <li><a href="<?php echo $urlLinkedin; ?>"target="_blank"><span class="icon-linkedin"></span></a></li>
                    <?php
                    endif;
                    if(!empty($urlInstagram)) :
                        ?>
                        <li><a href="<?php echo $urlInstagram; ?>" target="_blank"><span class="icon-instagram"></span></a></li>
                    <?php
                    endif;
                    ?>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script>
    var path = "<?php echo get_stylesheet_directory_uri(); ?>/";
    var ajaxurl =  "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/slick-carousel/slick/slick.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/wow/dist/wow.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/intl-tel-input/build/js/intlTelInput.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>