<?php
get_header();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;


if($paged == 1) :
    $args = array(
        'posts_per_page' => -1,
        'post__in'  => get_option( 'sticky_posts' ),
        'ignore_sticky_posts' => 1
    );
    $queryStiky = new WP_Query( $args );
    ?>
        <section class="hero">
            <div class="hero__slider">
                <?php
                $counter = 1;
                while($queryStiky->have_posts()) : $queryStiky->the_post();
                ?>
                    <div class="hero__slide">
                        <div class="hero__slide__background" style="background-image: url('<?php echo the_post_thumbnail_url('poststiky'); ?>');"></div>
                        <div class="row">
                            <div class="hero__slide__content">
                                <div class="hero__slide__content__attribute">
                                    <p>Artículo Destacado <?php echo $counter; ?></p>
                                </div>
                                <div class="hero__slide__content__title">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                                <div class="hero__slide__content__description">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                $counter++;
                endwhile;
                ?>
            </div>
            <div id="arrow-container" class="row arrow-container"></div>
        </section>
<?php
endif; // $paged == 1

$terms = get_terms(array(
    'taxonomy' => 'category',
    'orderby' => 'slug',
    'order' => 'ASC',
    //'parent' => '1',
    'hide_empty' => false
));
if(count($terms) > 0)
{
    ?>
    <section class="categories">
        <div class="row">
            <div class="categories__title">
                <h2>Categorías</h2>
            </div>
        </div>


            <div class="row">
                <div class="categories__items">
                    <nav>
                        <?php
                        foreach ($terms as $term) {
                            $categoryLink = get_category_link($term->term_id);
                            ?>
                            <a href="<?php echo $categoryLink; ?>"><?php echo $term->name; ?></a>
                            <?php
                        }
                        ?>
                    </nav>
                </div>
            </div>
    </section>
    <?php
}
$queryFeed = new WP_Query( array(
        'paged' => $paged,
        'post__not_in' => get_option( 'sticky_posts' )
) );
?>
    <section class="articles-feed">
        <div class="row">
            <?php
            while($queryFeed->have_posts()) : $queryFeed->the_post();
            ?>
            <article class="article-feed">
                <a href="<?php the_permalink(); ?>">
                    <div class="article-feed__background" style="background-image: url('<?php echo the_post_thumbnail_url('postfeed'); ?>');"></div>
                    <div class="article-feed__content">
                        <div class="article-feed__content__text-container">
                            <div class="article-feed__content__category">
                                <?php
                                $post_categories = get_the_category(get_the_ID());
                                //var_dump($post_categories);
                                ?>
                                <p>
                                    <?php
                                    $str = implode(', ', array_map(function($cat) {
                                        return $cat->name;
                                    }, $post_categories));
                                    echo $str;
                                    ?>
                                </p>
                            </div>
                            <div class="article-feed__content__title">
                                <h2><?php the_title(); ?></h2>
                            </div>
                            <div class="article-feed__content__excerpt">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                </a>
            </article>

            <?php
            endwhile;
            ?>
        </div>
    </section>

<?php
$big = 999999999; // need an unlikely integer
?>
    <section class="pagination">
        <div class="row">
            <?php
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $queryFeed->max_num_pages,
                'prev_next'          => true,
                'prev_text'          => __('« '),
                'next_text'          => __(' »'),
                'after_page_number'  => '',
                'type'               => 'list',
            ) );
            ?>
        </div>
    </section>

    <section class="subscription--red">
        <div class="row">
            <div class="subscription__title">
                <h2>¿Te gusta nuestro blog?</h2>
                <p>Suscríbete para recibir noticias</p>
            </div>
        </div>
        <div class="row">
            <div class="subscription__form">
                <div class="inner">
                    <form action="" id="form">
                        <div class="form-control">
                            <input type="email" class="email" name="email" placeholder="Email" required>
                            <button type="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
?>